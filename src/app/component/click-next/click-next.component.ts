import {Component, Input, OnDestroy, OnInit, Output, } from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ScrollService} from '../../services/ScrollService';
import {Router} from '@angular/router';

@Component({
  selector: 'click-next',
  templateUrl: 'click-next.component.html'
})
export class ClickNextComponent implements OnInit, OnDestroy {

  public activeNext = false;
  public subscriptionScroll: Subscription;

  @Input()
  public link;

  public constructor(private scrollService: ScrollService,private router: Router) {
  }

  public goToPage():void{
      this.router.navigate([this.link]);
      this.scrollService.setCurrentPosition(0);
  }

  

  public ngOnInit(): void {
    this.subscriptionScroll =  this.scrollService.scrollPercentDocumentSubject.subscribe(value =>{
        if(value > 90){
          this.activeNext = true;
        }else{
          this.activeNext = false;
        }
    });
    
 }

  public ngOnDestroy(): void {
    this.subscriptionScroll.unsubscribe();
  }

}
