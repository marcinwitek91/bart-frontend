import {Component, Input, OnDestroy, OnInit, Output, EventEmitter,ElementRef,HostListener} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ScrollService} from '../../services/ScrollService';

@Component({
  selector: 'gallery',
  templateUrl: 'gallery.component.html'
})
export class GalleryComponent implements OnInit, OnDestroy {

  @Input()
  public images: any[] = [];

  public scrollPosition = 0;
  public scrollEffect = 0;
  public subscriptionScroll: Subscription;
  public subscriptionEffect: Subscription;
  public heightDocument;


  public constructor(private scrollService: ScrollService,private el:ElementRef) {
  }

  public getElementScroll() {
      return '-'+this.scrollPosition+'px';
  }


  public ngOnInit(): void {
     this.scrollService.setCurrentPosition(0);

    this.subscriptionScroll = this.scrollService.scrollValueSubject.subscribe(value =>{
        this.scrollPosition = value;
    });
    this.subscriptionEffect = this.scrollService.scrollEffectSubejct.subscribe(value => {
         this.scrollEffect = value;
    });
    
    setTimeout(()=>{
      this.heightDocument = this.el.nativeElement.firstChild.offsetTop + this.el.nativeElement.firstChild.offsetHeight;
      this.scrollService.setHeightGallery(this.heightDocument);
    }, 500);

    
  }

  public ngOnDestroy(): void {
    this.subscriptionScroll.unsubscribe();
    this.subscriptionEffect.unsubscribe();
  }

}
