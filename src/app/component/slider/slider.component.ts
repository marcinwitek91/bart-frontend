import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {SliderService} from '../../services/SliderService';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'slider',
  templateUrl: 'slider.component.html'
})
export class SliderComponent implements OnInit, OnDestroy {

  public slides: any[] = [];

  public subscriptionSlider: Subscription;
  public currentIndex = -1;
  public oldIndex = -1;
  public timerId = null;

  public constructor(private sliderService: SliderService) {
    this.slides.push('assets/images/image1.jpg');
    this.slides.push('assets/images/image2.jpg');
    this.slides.push('assets/images/image3.jpg');
    this.slides.push('assets/images/image4.jpg');
    this.slides.push('assets/images/image1.jpg');
  }

  public ngOnInit(): void {
    this.subscriptionSlider = this.sliderService.sliderValueSubject.subscribe(index => {
        if(this.currentIndex === index) return;

        if(this.oldIndex === index){
          this.oldIndex = -1;
        }

        setTimeout(()=>{
          this.oldIndex = this.currentIndex;
          this.currentIndex = index;
        }, 10);

    });
  }

  public ngOnDestroy(): void {
      this.subscriptionSlider.unsubscribe();
  }

}
