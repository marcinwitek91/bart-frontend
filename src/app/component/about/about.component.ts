import {Component,Input, OnDestroy, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {fadeInAnimation} from '../../animations/fade-in.animation';


@Component({
  selector: 'about-page',
  templateUrl: 'about.component.html',
  animations: [fadeInAnimation]
})
export class AboutComponent implements OnInit, OnDestroy {
 
  @Input()
  public active;

  public fadeInState = 'enter';
  

  public constructor(private router: Router) {
   
  }

  public ngOnInit(): void {
      
  }

  public ngOnDestroy(): void {
  }
}