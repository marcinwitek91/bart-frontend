import {Component, Input, OnDestroy, OnInit, Output, EventEmitter, HostListener} from '@angular/core';
import {SliderService} from '../../services/SliderService';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';

@Component({
  selector: 'main-menu',
  templateUrl: 'main-menu.component.html'
})
export class MainMenuComponent implements OnInit, OnDestroy {

  @Input()
  public menuList: any[] = [];
  
  public subscriptionMenu: Subscription;
  public currentIndex = -1;

  public constructor(private sliderService: SliderService,private router: Router){

  }

  public changeSlider(index){
    this.sliderService.changeSlide(index);
  }

  public goToPage(value){
    this.router.navigate([value.toLowerCase()]);
  }

  public ngOnInit(): void {
    this.subscriptionMenu = this.sliderService.sliderValueSubject.subscribe(index =>{
        this.currentIndex = index;
    });

  }

  public ngOnDestroy(): void {
    this.subscriptionMenu.unsubscribe();
  }

}
