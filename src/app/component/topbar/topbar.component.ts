import {Component, Input, OnDestroy, OnInit, Output, EventEmitter,HostListener,ElementRef} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ScrollService} from '../../services/ScrollService';
import {Router} from '@angular/router';

@Component({
  selector: 'topbar',
  templateUrl: 'topbar.component.html'
})
export class TopBarComponent implements OnInit, OnDestroy {

  public subscriptionScroll: Subscription;
  public percentScroll = 0;
  public activeAbout = false;
  public activeContact = false;

  @Input()
  public color;

  public constructor(private router: Router,private scrollService: ScrollService) {
  }

  public getElementScroll() {
     return this.percentScroll+'%';
  }
  
  public setColor(){
    return this.color;
  }
  public fadeInPage(value){
    if(value == 'about'){
      this.activeAbout = true;
    }else if(value == 'contact'){
      this.activeContact = true;
    }
    
  }


  public ngOnInit(): void {
    this.subscriptionScroll =  this.scrollService.scrollPercentDocumentSubject.subscribe(value =>{
        this.percentScroll = value;
    });
    
 }

  public ngOnDestroy(): void {
    this.subscriptionScroll.unsubscribe();
  }

}
