import {Component, OnDestroy,Input, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {fadeInAnimation} from '../../animations/fade-in.animation';


@Component({
  selector: 'contact-page',
  templateUrl: 'contact.component.html',
  animations: [fadeInAnimation]
})
export class ContactComponent implements OnInit, OnDestroy {
 
  @Input()
  public active;
  public fadeInState = 'enter';

  public constructor(private router: Router) {
   
  }

  @HostListener('document:keyup', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      let x = event.keyCode;
      if (x === 27) {
         this.router.navigate(['/']);
    }
  }
  
  public ngOnInit(): void {
      
  }

  public ngOnDestroy(): void {
  }
}