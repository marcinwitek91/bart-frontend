import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './routes/app-routing.module';
import {HomePageComponent} from './pages/homepage/homepage.component';
import {TablesComponent} from './pages/tables/tables.component';
import {ChairsComponent} from './pages/chairs/chairs.component';
import {BookshelvesComponent} from './pages/bookshelves/bookshelves.component';
import {BedsComponent} from './pages/beds/beds.component';
import {CyclesComponent} from './pages/cycles/cycles.component';
import {AboutComponent} from './component/about/about.component';
import {ContactComponent} from './component/contact/contact.component';


import {MainMenuComponent} from './component/main-menu/main-menu.component';
import {SliderComponent} from './component/slider/slider.component';
import {GalleryComponent} from './component/gallery/gallery.component';
import {TopBarComponent} from './component/topbar/topbar.component';
import {ClickNextComponent} from './component/click-next/click-next.component';

import {SliderService} from './services/SliderService';
import {ScrollService} from './services/ScrollService';



import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [BrowserModule, BrowserAnimationsModule, FormsModule, AppRoutingModule],
  declarations: [AppComponent,
  HomePageComponent,
  MainMenuComponent,
  GalleryComponent,
  TopBarComponent,
  SliderComponent,
  TablesComponent,
  BookshelvesComponent,
  ChairsComponent,
  BedsComponent,
  CyclesComponent,
  AboutComponent,
  ContactComponent,
  ClickNextComponent
  ],
  bootstrap: [AppComponent],
  providers: [SliderService,ScrollService]
})
export class AppModule {
}
