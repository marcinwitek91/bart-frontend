import {Component, OnDestroy, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {ScrollService} from '../../services/ScrollService';


@Component({
  selector: 'chairs-page',
  templateUrl: 'chairs.component.html',
})
export class ChairsComponent implements OnInit, OnDestroy {
 
  public images: any[] = [];
  public color = "rgb(40,38,39)";
  public linkNext = 'bookshelves';
  public y;

  public constructor(private scrollService: ScrollService) {
   this.images.push('assets/images/image1.jpg');
   this.images.push('assets/images/image2.jpg');
   this.images.push('assets/images/image3.jpg');
   this.images.push('assets/images/image4.jpg');
  }

   @HostListener('mousewheel', ['$event'])
    onWhell(event: any) {
        this.scrollService.setCurrentPosition(event.deltaY);
    }

    @HostListener('DOMMouseScroll', ['$event'])
    onScroll(event: any) {
      var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
      this.scrollService.setCurrentPosition(100 * -(delta));
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.scrollService.setCurrentPosition(0);
    }
    @HostListener('touchstart', ['$event'])
    ontouchstart(event){
      this.y = event.changedTouches[0].clientY;
    }
    @HostListener('touchmove', ['$event'])
    ontouchmove(event){
      let delta = event.changedTouches[0].clientY - this.y;
      this.y = event.changedTouches[0].clientY;
      this.scrollService.setCurrentPosition(delta);
    }
  
  public ngOnInit(): void {
      
  }

   

  public ngOnDestroy(): void {
  }
}