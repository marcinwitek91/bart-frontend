import {Component, OnDestroy, OnInit, HostListener,ElementRef, Renderer2} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {SliderService} from '../../services/SliderService';

@Component({
  selector: 'homepage-page',
  templateUrl: 'homepage.component.html',
})
export class HomePageComponent implements OnInit, OnDestroy {
 
  public menuStructure: any[] = [];
  public backgroundColor: any[] = [];
  public subscriptionBackgroundColor: Subscription;
  public logo = 'assets/images/logo.png';
  public classWhite = false;
  public currentSlide;
  public activeAbout = false;
  public activeContact = false;

  public constructor(private router: Router,private el:ElementRef, private sliderService: SliderService,private renderer: Renderer2) {
    this.menuStructure.push('TABLES');
    this.menuStructure.push('CHAIRS');
    this.menuStructure.push('BOOKSHELVES');
    this.menuStructure.push('BEDS');
    this.menuStructure.push('CYCLES');

    this.backgroundColor.push("#f4f4f4");
    this.backgroundColor.push("rgb(40,38,39)");
    this.backgroundColor.push("rgb(212,93,11)");
    this.backgroundColor.push("rgb(155,169,157)");
    this.backgroundColor.push("rgb(136,202,30)");
  }

  public fadeInPage(value){
    if(value == 'about'){
      this.activeAbout = true;
    }else if(value == 'contact'){
      this.activeContact = true;
    }
    
  }

  public goToPage(value){
    this.router.navigate([value]);
  }

   @HostListener('mousewheel', ['$event'])
    onWhell(event: any) {
        if(event.deltaY > 0 && this.currentSlide < 4){
          this.sliderService.changeSlide(this.currentSlide+1);
        }else if(event.deltaY < 0 && this.currentSlide > 0){
          this.sliderService.changeSlide(this.currentSlide-1);
        }
        
    }
  @HostListener('DOMMouseScroll', ['$event'])
    onScroll(event: any) {
      var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
       if(100 * -(delta) > 0 && this.currentSlide < 4){
          this.sliderService.changeSlide(this.currentSlide+1);
        }else if(100 * -(delta) < 0 && this.currentSlide > 0){
          this.sliderService.changeSlide(this.currentSlide-1);
        }

    }

   @HostListener('document:keyup', ['$event'])
      handleKeyboardEvent(event: KeyboardEvent) {
        let x = event.keyCode;
        if (x === 27) {
          this.activeAbout = false;
          this.activeContact = false;
          if(this.currentSlide == 0){
            this.sliderService.changeSlide(1);
          }else{
            this.sliderService.changeSlide(0);
          }
          
      }
    } 
   



  public ngOnInit(): void {
    this.subscriptionBackgroundColor = this.sliderService.sliderValueSubject.subscribe(index => {
      this.renderer.setStyle(this.el.nativeElement.firstChild, "background-color", this.backgroundColor[index]);
      if(index < 5){
        this.currentSlide = index;
      }
      if(index > 0){
        this.logo = 'assets/images/bart-white.png'
        this.classWhite = true;
      }else{
        this.logo = 'assets/images/logo.png';
        this.classWhite = false;
      }
  });
      
  }


  public ngOnDestroy(): void {
    this.subscriptionBackgroundColor.unsubscribe();
  }
}