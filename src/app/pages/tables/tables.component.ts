import {Component, OnDestroy, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'tables-page',
  templateUrl: 'tables.component.html',
})
export class TablesComponent implements OnInit, OnDestroy {
 
  public images: any[] = [];
  public currentPosition = 0;
  public linkNext = 'chairs';

  public constructor() {
   
   this.images.push('assets/images/image1.jpg');
   this.images.push('assets/images/image2.jpg');
   this.images.push('assets/images/image3.jpg');
   this.images.push('assets/images/image4.jpg');
  }

  public ngOnInit(): void {
    
  }

  public ngOnDestroy(): void {
  }
}