import {Component, OnDestroy, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {ScrollService} from '../../services/ScrollService';


@Component({
  selector: 'bookshelves-page',
  templateUrl: 'bookshelves.component.html',
})
export class BookshelvesComponent implements OnInit, OnDestroy {
 
  public images: any[] = [];
  public color = "rgb(212,93,11)";
  public linkNext = 'beds';
  public y;

  public constructor(private scrollService: ScrollService) {
   
   this.images.push('assets/images/image1.jpg');
   this.images.push('assets/images/image2.jpg');
   this.images.push('assets/images/image3.jpg');
   this.images.push('assets/images/image4.jpg');
  }

   @HostListener('mousewheel', ['$event'])
    onWhell(event: any) {
        this.scrollService.setCurrentPosition(event.deltaY);
    }

    @HostListener('DOMMouseScroll', ['$event'])
    onScroll(event: any) {
      var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
      this.scrollService.setCurrentPosition(100 * -(delta));
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.scrollService.setCurrentPosition(0);
    }
    @HostListener('touchstart', ['$event'])
    ontouchstart(event){
      this.y = event.changedTouches[0].clientY;
    }
    @HostListener('touchmove', ['$event'])
    ontouchmove(event){
      let delta = event.changedTouches[0].clientY - this.y;
      this.y = event.changedTouches[0].clientY;
      this.scrollService.setCurrentPosition(delta);
    }
  
  public ngOnInit(): void {
      
  }

  public ngOnDestroy(): void {
  }
}