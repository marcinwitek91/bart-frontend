import {Component, OnDestroy, OnInit, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {ScrollService} from '../../services/ScrollService';


@Component({
  selector: 'cycles-page',
  templateUrl: 'cycles.component.html',
})
export class CyclesComponent implements OnInit, OnDestroy {
 
  public images: any[] = [];
  public color = 'rgb(136,202,30)';
  public y;

  public constructor(private scrollService: ScrollService) {
   
   this.images.push('assets/images/image1.jpg');
   this.images.push('assets/images/image2.jpg');
   this.images.push('assets/images/image3.jpg');
   this.images.push('assets/images/image4.jpg');
  }

    @HostListener('mousewheel', ['$event'])
    onWhell(event: any) {
        this.scrollService.setCurrentPosition(event.deltaY);
    }

    @HostListener('DOMMouseScroll', ['$event'])
    onScroll(event: any) {
      var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
      this.scrollService.setCurrentPosition(100 * -(delta));
    }

  
  public ngOnInit(): void {
      
  }

  public ngOnDestroy(): void {
  }
}