import {Component, OnDestroy, OnInit,HostListener} from '@angular/core';
import {ScrollService} from './services/ScrollService';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent implements OnInit, OnDestroy {


  public subscriptionHeight: Subscription;  

  constructor(private scrollService: ScrollService) {
  } 

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
    this.subscriptionHeight.unsubscribe();
  }

}
