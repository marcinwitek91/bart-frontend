import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from '../pages/homepage/homepage.component';
import {TablesComponent} from '../pages/tables/tables.component';
import {ChairsComponent} from '../pages/chairs/chairs.component';
import {BookshelvesComponent} from '../pages/bookshelves/bookshelves.component';
import {BedsComponent} from '../pages/beds/beds.component';
import {CyclesComponent} from '../pages/cycles/cycles.component';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'tables', component: TablesComponent},
  {path: 'chairs', component: ChairsComponent},
  {path: 'bookshelves', component: BookshelvesComponent},
  {path: 'beds', component: BedsComponent},
  {path: 'cycles', component: CyclesComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule
  ]
})
export class AppRoutingModule {
}
