import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';

export const fadeInAnimation = trigger('fadeInAnimation', [

  state('void', style({
    opacity: '0',
    transform: 'scale(0.5)'
  })),
  state('enter', style({
    opacity: '1',
    transform: 'scale(1)'
  })),
  transition('void => enter', animate('0.6s ease-in')),
  transition('* => void', animate('0s'))
]);
