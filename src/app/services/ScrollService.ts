import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class ScrollService {

  public scrollValueSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public scrollEffectSubejct: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public scrollPercentDocumentSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public currentPosition = 0;
  public heightDocument = 0;
  public percentScroll;

  constructor() {
  }

  public setCurrentPosition(value){
      if(this.currentPosition > 0 && this.heightDocument >= this.currentPosition ){
        this.currentPosition = this.currentPosition + value;
      }else if(this.currentPosition > 0 && value < 0){
        this.currentPosition = this.currentPosition + value;
      }else if(this.currentPosition <= 0 && value > 0){
        this.currentPosition = this.currentPosition + value;
      }

      if(value===0){
        this.currentPosition = 0;
      }
      this.countPercentScrollDocument();
      this.setEffectScroll(value);
      this.scrollValueSubject.next(this.currentPosition);
  }

  public setEffectScroll(value){
     if(value > 0){
          this.scrollEffectSubejct.next(1);
      }else if(value < 0){
        this.scrollEffectSubejct.next(2);
      }
      setTimeout(()=>{
        this.scrollEffectSubejct.next(0);
      }, 900);
  }

  public setHeightGallery(value){
      this.heightDocument = value;
  }

  public countPercentScrollDocument(){
      this.percentScroll = (this.currentPosition*100)/this.heightDocument;
      this.scrollPercentDocumentSubject.next(this.percentScroll);
  }


}
