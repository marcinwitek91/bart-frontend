import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SliderService {

  public sliderValueSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);


  constructor() {
  }

  public changeSlide(index){
      this.sliderValueSubject.next(index);
  }
}
