import { TestProjectCliPage } from './app.po';

describe('test-project-cli App', () => {
  let page: TestProjectCliPage;

  beforeEach(() => {
    page = new TestProjectCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
